'use strict';

const mongoose = require('mongoose');
const config = require('../lib/config');

mongoose.Promise = global.Promise;

let db;

function connect() {

  let opts = config.get('db:options');
  opts.useMongoClient = true;
  mongoose.connect(config.get('db:uri'), opts);
  db = mongoose.connection;
  db.on('error', console.error.bind(console, 'connection error:'));
  db.once('open', function() {
    console.log('DB connected');
  });
  //db.on('disconnect', connect);
}

connect();

module.exports = mongoose;

