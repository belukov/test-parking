'use strict';

const debug = require('debug')('test-parking:lib:import');
const fs = require('fs');
const readline = require('readline');
const calendarApi = require(__baseDir + '/lib/restapi/calendar');

let statuses = {};


function parseFile(fileName) {

  debug('parseFile(%s)', fileName);
  let filePath = __baseDir + '/tmp/' + fileName;
  debug('filePath ', filePath);
  statuses[fileName] = {
    state: 'pending',
    parsed: [],
    errors: []
  }
  let stream, rl;
  try {
    stream = fs.createReadStream(filePath);
    rl = readline.createInterface({
      input: stream,
      crlfDelay: Infinity
    });
  } catch(e) {
    console.error(e);
    statuses[fileName] = {
      state: 'error',
      error: e.message
    }
    throw e;
  }
  let lineNumber = 0;
  rl.on('line', async (line) => {
    debug('line: ', line);
    let data;
    try {
      data = JSON.parse(line);
    } catch(e) {
      //skip...
      return;
    }
    lineNumber++;
    let _curLine = lineNumber;
    debug('data: ', data);
    stream.pause();
    try {
      let id = await calendarApi.importRow(data);
      statuses[fileName].parsed.push([_curLine, id]);
    } catch(e) {
      statuses[fileName].errors.push([_curLine, e.message]);
    }
    stream.resume();
  });
  stream.on('end', () => {
    debug('finish ', statuses[fileName]);
    rl.close();
    statuses[fileName].state = 'parsed';

    
  });

  setTimeout(() => {
    // clear memory and fs
    delete statuses[fileName];
    fs.unlink(filePath);
  }, 1000 * 60 * 60 * 2).unref();

  return rl;

}

function fileStatus (fileName) {

  //debug('statuses', statuses);
  return statuses[fileName];
}

module.exports = {parseFile, fileStatus}


