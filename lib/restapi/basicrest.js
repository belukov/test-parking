'use strict';

const debug = require('debug')('test-parking:api:basic');

class BasicRestApi {
  

  constructor() {
    this.model = null;
  }

  get(filter = {}) {
    return this.model.find(filter);
  }

  async getOne(id) {
    debug('getOne', id);
    let doc = await this.model.findById(id);
    debug('result: ', doc);
    if (!doc) throw new Error("#" + id + " not found");
    return doc;
  }

  async add(data) {

    //debug('basicApi:add');
    //console.log('add to: ', this.model.collection.name);
    let doc = new this.model(data);
    try {
      let res = await doc.save();
      //console.log('added');
    } catch(e) {
      //console.log('err: ', e.message);
      throw(e);
    }
    return doc._id;
  }

  async upd(id, data) {
  
    let doc = await this.model.findById(id);
    if (!doc) {
      throw new Error('#' + id + ' not found');
    }
    doc.set(data);
    return doc.save();
  }

  async drop(id) {
  
    let doc = await this.model.findById(id);
    if (!doc) {
      throw new Error('#' + id + ' not exists');
    }
    return doc.remove();
  }
}

module.exports = BasicRestApi;
