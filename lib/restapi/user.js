'use strict';

const UserModel = require('../models/user');
const BasicRestApi = require('./basicrest');

class UserApi extends BasicRestApi{
  
  constructor() {
    super();
    this.model = UserModel;
  }
}

module.exports = new UserApi;
