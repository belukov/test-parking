'use strict';

const debug = require('debug')('test-parking:api:statistics');
const mongoose = require('mongoose');
const CEntryModel = require('../models/calendarentry');
const UserModel = require('../models/user');

class StatisticsApi {


  constructor() {
  
  }

  async userStatuses(_filter = {}) {

    let filter = {};

    for(let key in _filter) {
      let val = _filter[key];
      switch(key) {
        case 'userId': filter.user = mongoose.Types.ObjectId(val); break;
        case 'from':
          filter.date = {$gte: CEntryModel.parseDateStr(val)};
          break;
        case 'to':
          if (!filter.date) filter.date = {};
          filter.date['$lte'] = CEntryModel.parseDateStr(val);
          break;
        default: break;
      }
    }

  
    debug('userStatuses start. filter: ', filter);
    let aggrRes = await CEntryModel.aggregate()
      .match(filter)
      .group({
        _id: {
          user: "$user",
          state: "$state"
        },
        duration: {$sum: "$duration"}
      });
    debug('aggregate result: ', JSON.stringify(aggrRes, null, 2));
    let stats = [];
    let userIndex = {};
    for(let item of aggrRes) {
    
      let userId = item._id.user.toString();
      let state = item._id.state;
      let duration = item.duration;
      let row;
      if (!Number.isInteger(userIndex[userId])) {
        let user = await UserModel.findById(userId);
        //debug('user: ', userId, user);
        stats.push({
          userId,
          userEmail: (user ? user.email : null),
          userName: (user ? user.name : null),
          stat: {}
        });
        userIndex[userId] = stats.length - 1;
      }
      row = stats[userIndex[userId]];
      row.stat[state] = duration;
    }
    debug('userIndex: ', userIndex);
    debug('statistic result: ', JSON.stringify(stats, null, 2));
    return stats;
  }
}

module.exports = new StatisticsApi;
