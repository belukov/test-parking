'use strict';

const debug = require('debug')('test-parking:api:calendar');
const CEntryModel = require('../models/calendarentry');
const UserModel = require('../models/user');
const BasicRestApi = require('./basicrest');

class CalendarApi extends BasicRestApi{
  
  constructor() {
    super();
    this.model = CEntryModel;
    this.defaultLimit = 1000;
  }

  get(_filter = {}, skip = 0, limit = this.defaultLimit) {

    let filter = makeCalendarFilter(_filter);

    debug('calendar filter: ', filter);
    return super.get(filter)
      .populate('user')
      .sort({date: 1})
      .limit(limit)
      .skip(skip);
  }

  getCount(_filter) {

    let filter = makeCalendarFilter(_filter);
    return super.get(filter).count()
  }

  async getOne(id) {
    let doc = await this.model.findById(id).populate('user');
    if (!doc) throw new Error("#" + id + " not found");
    return doc;
  }

  async add(data) {

    debug('calendarApi:add');
    if (!data.date) throw new Error('date not set');
    let arr = data.date.split('-');
    if (arr.length == 1) {
      return super.add(data);
    }

    let period = CEntryModel.parseDatePeriod(arr[0], arr[1]);
    debug('parsed period: ', period);
    if (period.length == 1) {
    }
    debug('add by period');

    // validate simple
    let first = this.model(Object.assign({}, data, {date: period[0]}));
    let err = await first.validate();
    if (err) throw(err);

    // try to add each
    let added = [];

    let itemDate;
    while (itemDate = period.shift()) {
      debug('itemDate: ', itemDate.toDateString());
    
      let itemData = Object.assign({}, data, {date: itemDate});
      let doc =  this.model(itemData);
      try {
        await doc.save();
        added.push(doc);
      } catch(e) {
        e.message = "[" + doc.dateString + "] " + e.message;
        for(let _doc in added) {
          await _doc.remove();
        }
        throw e;
      }
    }
    return added.map((doc) => doc._id);
  }

  async importRow(data) {
    
    let user = await UserModel.findOne({email: data.email});
    if (!user) {
      user = UserModel({
        email: data.email,
        name: data.name
      });
      await user.save();
    }
    let entry = this.model({
      date: data.date,
      state: data.status,
      duration: 'full',
      user: user._id
    });
    await entry.save();
    return entry.id;
  }
}

module.exports = new CalendarApi;


function makeCalendarFilter (_filter) {

  let filter = {};
  for(let key in _filter) {
    let val = _filter[key];
    switch(key) {
      case 'userId': filter.user = val; break;
      case 'from':
        filter.date = {$gte: CEntryModel.parseDateStr(val)};
        break;
      case 'to':
        if (!filter.date) filter.date = {};
        filter.date['$lte'] = CEntryModel.parseDateStr(val);
        break;
      case 'state':
      case 'id':
      //case 'duration':
        filter[key] = val;
        break;
      default: break;
    }
  }
  return filter;

}
