'use strict';

const debug = require('debug')('test-parking:lib:models:calendar');
//const mongoose = require('../mongoose');
const mongoose = require('mongoose');

const workdayLength = 8;

const durationAliases = {
  full: workdayLength,
  half: workdayLength / 2,
  fourth: workdayLength / 4
}

const CEntrySchema = new mongoose.Schema({
  date: {
    type: Date,
    required: true
  },
  user: {
    type: mongoose.Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },
  state: {
    type: String,
    enum: ['work', 'vacation', 'sick', 'remoteWork'],
    required: true
  },
  duration: {
    type: Number,
    default: workdayLength,
    min: 1,
    max: workdayLength
  },
  note: String
});

CEntrySchema.path('duration').set(function(v) {
  if (durationAliases[v]) {
    v = durationAliases[v];
  }
  return Math.ceil(Number(v));
});

CEntrySchema.path('date').set(function(v) {
  if ('string' == typeof v) {
    v = this.constructor.parseDateStr(v);
  }
  return v;
})

CEntrySchema.virtual('durationString').get(function() {

  let value = '';
  for (let key in durationAliases) {
    if (durationAliases[key] == this.duration) value = key;
  }
  if ('' === value) {
    value = this.duration + ' hours';
  }
  return value;
});

CEntrySchema.virtual('dateString').get(function() {
  return [
      String(this.date.getDate()).padStart(2, '0'),
      String(this.date.getMonth() + 1).padStart(2, '0'),
      this.date.getFullYear()
    ].join('.');
});

CEntrySchema.pre('save', function _checkDuration(next) {

  //debug('check duration summ bewfore save');
  this.constructor.aggregate()
    .match({
      date: this.date,
      user: this.user,
      _id: {$ne: this._id}
    })
    .group({
      _id: "$user",
      totalDuration: {$sum: "$duration"}
    }).exec((err, aggr) => {
      if (err) return next(err);
      //debug('aggr: ', aggr, this.duration);

      if (!aggr.length) return next();

      let storedDur = aggr[0].totalDuration;
      let summ = storedDur + this.duration;
      if (summ <= workdayLength) return next();

      let flderr = new Error("Total day duration more than work day");
      this.invalidate('duration', flderr);
      return next(flderr);
    });
});


CEntrySchema.static('parseDateStr', (val) => {

  //debug('parse date', val);
  if (Date == typeof val) return val;
  let regexp = /0?(\d\d?)\.0?(\d\d?)\.(\d\d\d\d)/i;
  let match = val.match(regexp);
  //debug('match: ', match);
  if (!match) return null;
  //debug('date parts: ', Number(match[3]), Number(match[2]) - 1, Number(match[1]));
  let d = new Date(Number(match[3]), Number(match[2]) - 1, Number(match[1]), 0, 0, 0);
  //debug('date: ', d.toDateString());
  return d;
});

CEntrySchema.static('parseDatePeriod', function (from, to) {

  //debug('parseDatePeriod(%s, %s)', from, to);
  from = (typeof from == Date) ? from : this.parseDateStr(from);
  if (!from) throw new Error('Incorrect fromat `from`');

  let period = [from];
  if (to) {
    to = (typeof to == Date) ? to : this.parseDateStr(to);
    if (!to) throw new Error('Incorrect format `to`');

    if (from > to) throw new Error("`from` must be less then `to`");
    let _cur = new Date(from);
    while (_cur < to) {
      _cur.setDate(_cur.getDate() + 1);
      //debug('cur : ', _cur.toDateString());
      period.push(new Date(_cur));
    }
  }
  //debug('period: ', period);
  return period;
});

module.exports = mongoose.model('CalendarEntry', CEntrySchema);
