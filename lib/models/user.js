'use strict';

const debug = require('debug')('test-parking:lib:models:user');
//const mongoose = require('../mongoose');
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  phone: String,
  note: String
});

module.exports = mongoose.model('User', UserSchema);
