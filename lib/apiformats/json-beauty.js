'use strict';

module.exports = function (data, response) {

  response.set('Content-Type', 'application/json-stringified');
  return JSON.stringify(data, null, 2);
}
