* GET /api/users/ - список пользователей
* POST /api/users/ - добавление пользователя {name, email, phone, note}
* GET /api/users/:id - просмотр одного пользователя
* PUT /api/users/:id - изменение
* DELETE /api/users/:id - удаление

* /api/calendar/ - те же действия с записями календаря +
* GET /api/calendar/
	* допускаются фильтры userId, from, to, state, id
	* навигация:
		* limit - кол-во записей на страницу. default = 1000
		* page - нужная страница начиная с первой
	* в ответе выдается информация - лимит, текущая страница, максимальная страница
			
* POST /api/calendar/ 
{
		date: '01.02.2017' || '01.02.2017 - 10.02.2017', // во втором случае добавит 10 записей
		user: <userId из бд>,
		state: <'work', 'vacation', 'sick', 'remoteWork'>,
		duration: <'full', 'half', 'fourth' или кол-во часов от 1 до 8> // продолжительность раб. дня хранится в константе,
		note: 'любая строка'
}
* POST /api/calendar/import - загрузка файла импорта
* GET /api/calendar/import/<код файла> - просмотр статуса обработки файла. коды файлов возвращаются при их загрузке

* GET /statistics/user-statuses - вывод статистики по пользователям
	* допускаются фильтры userId, from, to