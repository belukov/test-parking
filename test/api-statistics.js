'use strict';

const debug = require('debug')('test-parking:test:statistics-api');
const assert = require('assert');
//const mongoose = require('../lib/mongoose');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;

const CEntryModel = require('../lib/models/calendarentry'); 
const UserModel = require('../lib/models/user'); 

const api = require('../lib/restapi/statistics');

describe("Test statistics", () => {

  before(async () => {
    await mongoose.connect('mongodb://localhost:27017/test-parking', {useMongoClient: true});
  });
  after(() => {
    mongoose.connection.close();
  });

  describe("Test user-status statistics", () => {
  
    let user1, user2;

    before(async () => {
      await CEntryModel.remove({});
      await UserModel.remove({});

      user1 = await UserModel({name: '01', email: 'test-1@loc.loc'}).save();
      user2 = await UserModel({name: '02', email: 'test-2@loc.loc'}).save();

      debug('users: ', user1._id, user2._id);

      await fillEntryData([user1._id, user2._id], [
        '01.12.2016',
        '02.12.2016',
        '03.12.2016',
        '04.12.2016',
        '05.12.2016',
        '06.12.2016',
        '07.12.2016',
        ], 'work', 'full');

       await fillEntryData(user1._id, [
        '08.12.2016',
        '09.12.2016',
        '10.12.2016',
        '11.12.2016',
        '12.12.2016',
        '13.12.2016',
        '14.12.2016',
        ], 'work', 'full');

       await fillEntryData(user2._id, [
        '08.12.2016',
        '09.12.2016',
        '10.12.2016',
        '11.12.2016',
        '12.12.2016',
        '13.12.2016',
        '14.12.2016',
        ], 'sick', 'full');

       await fillEntryData(user1._id, [
        '15.12.2016',
        '16.12.2016',
        '17.12.2016',
        '18.12.2016',
        '19.12.2016',
        '20.12.2016',
        '21.12.2016',
        ], 'remoteWork', 'half');

       await fillEntryData(user2._id, [
        '15.12.2016',
        '16.12.2016',
        '17.12.2016',
        '18.12.2016',
        '19.12.2016',
        '20.12.2016',
        '21.12.2016',
        ], 'work', 'half');
    });

    it("test data loaded", async () => {
      let list = await CEntryModel.find();
      assert.equal(42, list.length);
    });
 

    it("Check full statistic", async () => {
    
      let stats = await api.userStatuses();
      assert.equal()
      for (let item of stats) {
      
        switch(item.userId) {
        
          case user1.id:
            assert.equal(112, item.stat.work);
            assert.equal(28, item.stat.remoteWork);
            break;
          case user2.id:
            assert.equal(84, item.stat.work);
            assert.equal(56, item.stat.sick);
            break;
          default:
            assert(false, "unknown user!");
            break;
        }
      }
    });

    it("Must show statistic only for user1", async () => {

      let stats = await api.userStatuses({userId: user1._id});
      debug('only user1 stats: ', JSON.stringify(stats, null, 2));
      assert.equal(1, stats.length);
      let item = stats[0];
      debug('only user1 stats item: ', JSON.stringify(item, null, 2));
      assert.equal(112, item.stat.work);
      assert.equal(28, item.stat.remoteWork);
    });

    it("Must show statistic only for week 2", async () => {

      let stats = await api.userStatuses({
        from: '08.12.2016',
        to: '14.12.2016'
        });
      for (let item of stats) {
      
        switch(item.userId) {
        
          case user1.id:
            assert.equal(56, item.stat.work);
            assert.equal(1, Object.keys(item.stat).length);
            break;
          case user2.id:
            assert.equal(56, item.stat.sick);
            assert.equal(1, Object.keys(item.stat).length);
            break;
          default:
            assert(false, "unknown user!");
            break;
        }
      }



    });


  });

});

async function fillEntryData(user, date, state, duration) {

  if (Array.isArray(user)) {
    for(let u of user) {
      await fillEntryData(u, date, state, duration);
    }
    return;
  }
  if (Array.isArray(date)) {
    for(let d of date) {
      await fillEntryData(user, d, state, duration);
    }
    return;
  }

  let data = {
    user,
    date,
    state,
    duration
  };
  //debug('add entry: ', data);
  let doc = CEntryModel(data);
  await doc.save();
  return;
}
