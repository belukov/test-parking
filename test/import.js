'use strict';

global.__baseDir = __dirname + '/../';
const debug = require('debug')('test-parking:test:import');
const assert = require('assert');
//const mongoose = require('../lib/mongoose');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const CEntryModel = require('../lib/models/calendarentry'); 
const UserModel = require('../lib/models/user'); 

const fileName = 'test_import_file';

const importLib = require('../lib/import');

describe('Test import', () => {

  before(async () => {
  
    await mongoose.connect('mongodb://localhost:27017/test-parking', {useMongoClient: true});
    await CEntryModel.remove({});
    await UserModel.remove({});
  });

  after(() => {
    mongoose.connection.close();
  });

  it ("Must import simple file", (done) => {
 
    let rl = importLib.parseFile(fileName);
    assert(rl);

    rl.on('close', () => {
      return done();
    });

    rl.on('error', done);
  });

  
  it ("Get file status", (done) => {
  
    let stat = importLib.fileStatus(fileName);
    assert.equal('parsed', stat.state);
    return done();
  });
  
});

