'use strict';

const assert = require('assert');
//const mongoose = require('../lib/mongoose');
const mongoose = require('mongoose');
const CEntryMdl = require('../lib/models/calendarentry');

describe("Test calendar specific things", () => {


  before(async () => {
  
    mongoose.connect('mongodb://localhost:27017/test-parking', {useMongoClient: true});
  });
  after(() => {
    mongoose.connection.close();
  });

  it("Must use duration aliases", async () => {
    let doc = new CEntryMdl({
      date: new Date,
      user: mongoose.Types.ObjectId(),
      state: 'work',
      duration: 'half'
    });
    assert.equal(4, doc.duration);
    doc.duration = 'fourth';
    assert.equal(2, doc.duration);
  });

  it("Must show correct durationString", () => {
  
    let doc = new CEntryMdl({});
    assert.equal('full', doc.durationString);
    doc.duration = 4;
    assert.equal('half', doc.durationString);
    doc.duration = 6;
    assert.equal('6 hours', doc.durationString);
  });

  it("Must parse correct date strings", () => {
  
    let date = CEntryMdl.parseDateStr('11.01.2018');
    assert.equal('Thu Jan 11 2018', date.toDateString());

    date = CEntryMdl.parseDateStr('abra-kodabra');
    assert.equal(null, date);
  });

  it("Must parse period 11.01.2018 - 15.01.2018", () => {
    let period = CEntryMdl.parseDatePeriod('11.01.2018', '15.01.2018');
    assert.equal(5, period.length);
  });

  it("Must parse period with only from 11.01.2018", () => {
    let period = CEntryMdl.parseDatePeriod('11.01.2018');
    assert.equal(1, period.length);
  });

  it("Must parse dificult period 29.12.2017 - 01.02.2018", () => {
    let period = CEntryMdl.parseDatePeriod('29.12.2017', '1.02.2018');
    assert.equal(35, period.length);
  });

  it("Must break if `from` incorrect", () => {
    assert.throws(() => {
      let period = CEntryMdl.parseDatePeriod('aa.01.2018', '15.01.2018');
    });
  });

  it("Must break if `to` less than `from` ", () => {
    assert.throws(() => {
      let period = CEntryMdl.parseDatePeriod('17.01.2018', '15.01.2018');
    });
  });




});
