'use strict';

const debug = require('debug')('test-parking:test:calendar-api');
const assert = require('assert');
//const mongoose = require('../lib/mongoose');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const api = require('../lib/restapi/calendar');


describe("Test calendar api library", () => {

  before(async () => {
  
    await mongoose.connect('mongodb://localhost:27017/test-parking', {useMongoClient: true});
    //await mongoose.connection.dropCollection('calendarentries');
    await api.model.remove({});
  });
  after(() => {
    mongoose.connection.close();
  });

  const users = [
    mongoose.Types.ObjectId(),
    mongoose.Types.ObjectId()
  ];

  it("Must add one simple record", async () => {
  
    let id = await api.add({
      date: '01.12.2017',
      user: users[0],
      state: 'work'
    });
    //console.log('calend item id: ', id);
    assert(mongoose.Types.ObjectId.isValid(id));
  });

  it ("Must add two records of half day for one user and one day ", async () => {
  
    let rec1 = await api.add({
      date: '02.12.2017',
      user: users[0],
      state: 'work',
      duration: 4
    });
    let rec2 = await api.add({
      date: '02.12.2017',
      user: users[0],
      state: 'sick',
      duration: 4
    });
    assert(mongoose.Types.ObjectId.isValid(rec1));
    assert(mongoose.Types.ObjectId.isValid(rec2));

  });

  it("Must not allow add two records for same user and day with full and half day", async () => {
 
    let err = null;
    try {
       let rec1 = await api.add({
        date: '03.12.2017',
        user: users[0],
        state: 'work',
        duration: 4
      });
      let rec2 = await api.add({
        date: '03.12.2017',
        user: users[0],
        state: 'sick',
        duration: 8
      });
    } catch(e) {
      err = e;
    }
    assert(err, "Added two records with total duration more than work day");
  });

  it("Must add two records for different users in a same day and with 'full' day", async () => {

     let rec1 = await api.add({
      date: '04.12.2017',
      user: users[0],
      state: 'work',
      duration: 'full'
    });
    let rec2 = await api.add({
      date: '04.12.2017',
      user: users[1],
      state: 'work',
      duration: 'full'
    });
    assert(mongoose.Types.ObjectId.isValid(rec1));
    assert(mongoose.Types.ObjectId.isValid(rec2));
   
  });

  it ("Must correctly update `state` for fullday record ", async () => {

    let rec1 = await api.add({
      date: '05.12.2017',
      user: users[0],
      state: 'work',
      duration: 'full'
    });
    assert(mongoose.Types.ObjectId.isValid(rec1));
    await api.upd(rec1, {state: 'sick'});
    let doc = await api.getOne(rec1);
    assert.equal('sick', doc.state);
  });

  it("Must not update date if in new date duration exceed work day", async () => {

    let rec1 = await api.add({
      date: '06.12.2017',
      user: users[0],
      state: 'work',
      duration: 'full'
    });
    let rec2 = await api.add({
      date: '07.12.2017',
      user: users[0],
      state: 'work',
      duration: 'full'
    });
    assert(mongoose.Types.ObjectId.isValid(rec1));
    assert(mongoose.Types.ObjectId.isValid(rec2));

    let err = null;
    try {
      await api.upd(rec2, {date: '06.12.2017'});

    } catch(e) {
      assert.notEqual(-1, e.message.indexOf('duration more than'));
      err = e;
    }
    assert(err, "No error throw");
 
  });

  it("Must add 5 records by period", async () => {
  
    let data = {
      date: '08.12.2017 - 12.12.2017',
      user: users[1],
      state: 'work',
      duration: 'full'
    };
    let result = await api.add(data);
    debug('res: ', result);
    assert.equal(5, result.length);
  });

});
