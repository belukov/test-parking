'use strict';

const assert = require('assert');
//const mongoose = require('../lib/mongoose');
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
const userApi = require('../lib/restapi/user');

describe("Test user api library", () => {

  before(async () => {
  
    await mongoose.connect('mongodb://localhost:27017/test-parking', {useMongoClient: true});
    //await mongoose.connection.dropCollection('users');
    await userApi.model.remove({});
  });

  after(() => {
    mongoose.connection.close();
  });

  it("Lib must have methods", () => {
    assert.equal('function', typeof userApi.add);
    assert.equal('function', typeof userApi.get);
    assert.equal('function', typeof userApi.upd);
    assert.equal('function', typeof userApi.drop);

  });

  it("Must add test01 user", async () => {
  
    let userId = await userApi.add({
      name: 'test01',
      email: 'test01@local.local'
    });
    assert(mongoose.Types.ObjectId.isValid(userId));
  });

  it("Must NOT add user with same email", async () => {
  
   
    /*
    * Конструкция вида 
    * assert.thrown( async () => {}) 
    * не работает корректно
    */
    //console.log('adding duplicate...');
    let err = null;
    try {
      let _id = await userApi.add({
        name: 'test01-copy',
        email: 'test01@local.local'
      });
    } catch(e) {
      assert.notEqual(-1, e.message.indexOf('duplicate key'));
      err = e;
    }
    assert(err, "Added duplicated user");
  });

  it("Must not add user with empty name", async () => {
 
    let err = null;
    try {
      await userApi.add({
        name: '',
        email: 'incorrect@local.local'
      });
    } catch(e) {
      assert.notEqual(-1, e.message.indexOf("Path `name` is required"));
      err = e;
    }
      assert(err, "Added unnamed user");
  });

  it("There must be only one record now", async () => {
  
    let list = await userApi.get();
    assert.equal(1, list.length);
    let first = list.shift();
    assert.equal('test01@local.local', first.email);
  });

  it("Must add test02 and test03 users", async () => {
  
    await userApi.add({
      name: 'test02',
      email: 'test02@local.local'
    });
    await userApi.add({
      name: 'test03',
      email: 'test03@local.local'
    });
    let list = await userApi.get();
    assert.equal(3, list.length);
  });

  it("Must update test01 and set phone and note", async () => {
 
    let list;
    list = await userApi.get({name: 'test01'});
    assert.equal(1, list.length);
    let id = list[0]._id;
    await userApi.upd(id, {
      phone: '9999999',
      note: 'test note'
    });
    list = await userApi.get({name: 'test01'});
    assert.equal(1, list.length);
    assert.equal('9999999', list[0].phone);
    assert.equal('test note', list[0].note);
  });

  it("Must throw when try to update nonexistent user", async () => {
 
    let err = null;
    try {
      await userApi.upd(mongoose.Types.ObjectId(), {note: 'this is imposible!'});
    } catch(e) {
      assert.notEqual(-1, e.message.indexOf('not found'));
      err = e;
    }
    assert(err, "User not exist, but updated");
  });

  it("Must drop test02", async () => {
  
    let list = await userApi.get({name: 'test02'});
    assert.equal(1, list.length);
    let id = list[0].id;
    await userApi.drop(id);
    list = await userApi.get();
    assert.equal(2, list.length);

  });
  
});
