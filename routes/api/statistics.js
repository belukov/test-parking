'use strict';

const debug = require('debug')('test-parking:routes:api:statistics');
const express = require('express');
const router = express.Router();
const statApi = require(__baseDir + '/lib/restapi/statistics');


router.get('/user-statuses', async (req, res, next) => {

  let filter = req.query || {};
  debug('filter: ', filter);
  let stats = await statApi.userStatuses(filter);
  res.send({
    success: true,
    data: stats
  });
});

module.exports = router;


