'use strict';

const debug = require('debug')('test-parking:routes:api:users');
const express = require('express');
const router = express.Router();
const userApi = require(__baseDir + '/lib/restapi/user');

const userListFields = ['id', 'name', 'email'];
const userDetailFields = ['id', 'name', 'email', 'note'];

router.param('id', (req, res, next, id) => {
  req.userId = id;
  return next()
});

router.get('/', async (req, res, next) => {

  debug('get users list start');
  let userList = await userApi.get();
  debug('userList from db', userList);
  let list = userList.map((item) => {
    return filterObject(item, userListFields);
  });
  let result = {
    success: true,
    data: list
  }
  //debug('result', result);
  res.send(result);
});

router.get('/:id', async (req, res, next) => {

  debug('getOne start', req.userId);
  try {
    let user = await userApi.getOne(req.userId);
    debug('user: ', user);
    let result = filterObject(user, userDetailFields);
    res.send({
      success: true,
      data: result
    });
  } catch(e) {
    return next(e);
  }
});

router.post('/', async (req, res, next) => {

  debug('add user start');
  debug('body: ', req.body, typeof req.body);

  let data = req.body || {};
  let result = {
    success: false,
    error: '',
    data: {}
  }
  try {
    let id = await userApi.add(data);
    debug('added: ', id);
    result.success = true;
    result.data.id = id;
  } catch(e) {
    return next(e);
  }

  res.send(result);
});

router.put('/:id', async (req, res, next) => {

  debug('start update user ', req.userId, req.body);

  let data = req.body || {};
  try {
    let updResult = await userApi.upd(req.userId, req.body);
    res.send({
      success: true
    });
  } catch(e) {
    return next(e);
  }
});

router.delete('/:id', async (req, res, next) => {

  try {
    let delRes = await userApi.drop(req.userId);
    debug('delete result: ', delRes);
    res.send({
      success: true
    });
  } catch(e) {
    return next(e);
  }
});

module.exports = router;



/**
* Filter object properties by allowed keys list
*/
function filterObject(obj, keys) {

  let result = keys.reduce((res, key) => {
    res[key] = obj[key];
    return res;
  }, {});
  return result;
}


