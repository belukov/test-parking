'use strict';

const debug = require('debug')('test-parking:routes:api:calendar');
const express = require('express');
const router = express.Router();
const calendarApi = require(__baseDir + '/lib/restapi/calendar');
const fs = require('fs');
const importLib = require(__baseDir + '/lib/import');

router.param('id', (req, res, next, id) => {
  req.calendarEntryId = id;
  return next()
});


router.get('/', async (req, res, next) => {

  let filter = req.query || {};
  debug('request params', req.query);

  let total = await calendarApi.getCount(filter);

  // calc limits
  let limit = parseInt(req.query.limit) || calendarApi.defaultLimit;
  let maxPage = Math.ceil(total / limit);
  let page = parseInt(req.query.page) || 1;
  if (page > maxPage) page = maxPage; // throw error ?
  let skip = (page - 1) * limit;

  let list = await calendarApi.get(filter, skip, limit);
  debug('maxPage: = Math.ceil(%s / %s) = %s ', total, limit, Math.ceil(total / limit))
  let result = {
    success: true,
    page: page,
    maxPage: maxPage,
    limit: limit,
    data: list.map(makeEntryRow)
  }

  res.send(result);
});

router.get('/:id', async (req, res, next) => {
  debug('get one calendar item', req.calendarEntryId);
  try {
    let entry = await calendarApi.getOne(req.calendarEntryId);
    let result = makeEntryRow(entry);
    res.send({
      success: true,
      data: result
    });
  } catch(e) {
    return next(e);
  }
});

router.post('/', async (req, res, next) => {

  let data = req.body || {};

  try {
    let ids = await calendarApi.add(data);
    res.send({
      success: true,
      data: ids
    });
  } catch (e) {
    return next(e);
  }
});

router.put('/:id', async (req, res, next) => {

  let data = req.body || {};
  try {
    let updResult = await calendarApi.upd(req.calendarEntryId, req.body);
    res.send({
      success: true
    });
  } catch(e) {
    return next(e);
  }
});

router.delete('/:id', async (req, res, next) => {

  try {
    let delRes = await calendarApi.drop(req.calendarEntryId);
    res.send({
      success: true
    });
  } catch(e) {
    return next(e);
  }
});

router.post('/import', async (req, res, next) => {

  debug('files: ', req.files);
  if (!req.files) throw new Error("No file");
  let uploaded = {};
  for (let fileName in req.files) {
    let file = req.files[fileName];
    let uniqName = Math.random().toString(36).substring(2);
    try {
      await file.mv(__baseDir + '/tmp/' + uniqName);
      uploaded[fileName] = uniqName;
    } catch(e) {
      for(let fname in uploaded) {
        await fs.unlink(__baseDir + '/tmp/' + uploaded[fname]);
      }
      throw e;
    }
  }

  // TODO: run parser
  for (let fName in uploaded) {
    importLib.parseFile(uploaded[fName]);
  }

  res.send({
    success: true,
    message: 'Your files uploaded and will be parsed in background. See status at /api/import/<fileUniqueKey>',
    data: uploaded
  });

});

router.param('file', (req, res, next, fileName) => {
  req.fileName = fileName;
  return next();
});
router.get('/import/:file', async (req, res, next) => {

  let state = importLib.fileStatus(req.fileName) || null;
  res.send({
    success: state ? true : false,
    data: state
  });
});





module.exports = router;

function makeEntryRow(item) {
  return {
    id: item.id,
    date: item.dateString,
    state: item.state,
    duration: item.duration,
    durationString: item.durationString,
    userId: item.user ? item.user.id : null,
    userName: item.user ? item.user.name : null,
    userEmail: item.user ? item.user.email : null
  }
}
