'use strict';

const debug = require('debug')('test-parking:routes:api:index');
const express = require('express');
const router = express.Router();

const allowedTypes = {
  'application/json': require(__baseDir + '/lib/apiformats/json'),
  'application/jsonb': require(__baseDir + '/lib/apiformats/json-beauty') // usage example
};

router.use(function _checkAccept (req, res, next) {

  let accept = req.get('Accept');
  debug('Accept: ', accept);
  let formatter = allowedTypes[accept] || null;
  if (!formatter) {
    let err = new Error('Unsupported Accept type. Use one of: ' + Object.keys(allowedTypes).join(', '));
    err.code = 400;
    return next(err);
  }
  let originSend = res.send;
  res.send = function(data) {
    let args = Array.prototype.slice.call(arguments);
    data = formatter(data, res);
    args[0] = data;
    return originSend.apply(this, args);
  }
  return next();
  
});

router.get('/', (req, res, next) => {

  res.send({
    message: 'this is a root route for api',
  });
});

router.use('/users/', require('./users'));
router.use('/calendar/', require('./calendar'));
router.use('/statistics/', require('./statistics'));


/**
* Unknown uri
*/
router.use((req, res, next) => {
  let result = {
    success: false,
    error: "Unknown address or method"
  };
  res.send(result);
});

/**
* Catch route errors
*/
router.use((err, req, res, next) => {

  let result = {
    success: false,
    error: err.message
  };
  res.send(result);
});

module.exports = router;
